## ChangeWall.sh

### changewall.sh is a script for automatic change wallpaper in Gnome Shell.

* You need to copy changewall.sh script in ~/bin/ in your user directory.

* Change the path `walls_dir` variable on the script changewall.sh, if necessary, and the path `ExecStart` variable on the systemd unit gnome-wallpaper.service.

* Copy `.config/systemd/user/gnome-wallpaper.service` and `.config/systemd/user/gnome-wallpaper.timer` in ~/.config/systemd/user/ in your user directory.

* If you want to change the time period for changing the wallpaper, change the `OnCalendar` variable in the `~/.config/systemd/user/gnome-wallpaper.timer` file. 

* You need to reload systemd with `systemctl --user daemon-reload` then start the systend timer with `systemctl --user start gnome-wallpaper.timer`

* You can enable ChangeWall.sh at system startup like this: `systemctl --user enable gnome-wallpaper.service` and `systemctl --user enable gnome-wallpaper.timer`
